/*
 * @Author: “1077321622@qq.com” lzr448470520
 * @Date: 2023-11-23 10:50:21
 * @LastEditors: “1077321622@qq.com” lzr448470520
 * @LastEditTime: 2023-11-23 11:21:52
 * @FilePath: \Vue3开发常用插件集合\插件\src\main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import * as ElIconModules from '@element-plus/icons-vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'

const app = createApp(App)
app.use(ElementPlus)
app.use(createPinia())
app.use(router)


//图标
Object.keys(ElIconModules).forEach(function(key) {
    app.component(ElIconModules[key].name, ElIconModules[key])
  })  


app.mount('#app')
