

import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/Home.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/Home.vue'),//这里是核心： center作为容器组件，包含左右菜单和右侧的router-view，所以下面的子组件实际是显示在router-view
      redirect: { path: "/imgcropper" },   //输入路由center会重定向到userinfo页面
      children: [
        {
          path: "/imgcropper",
          name: "imgcropper",
          meta:{
            title:"图片裁剪",
          },
          component: () => import("../views/imgcropper.vue"),
        },
        {
          path: "/tinymce",
          name: "tinymce",
          meta:{
            title:"富文本",
          },
          component: () => import("../views/tinymce.vue"),
        },
        {
          path: "/dayjs",
          name: "dayjs",
          meta:{
            title:"datjs",
          },
          component: () => import("../views/dayjs/dayjs.vue"),
        },

        {
          path: "/pdf",
          name: "pdf",
          meta:{
            title:"pdf",
          },
          component: () => import("../views/pdf/pdf.vue"),
        },

        
        {
          path: "/father-son",
          name: "father-son",
          meta:{
            title:"父子通讯",
          },
          component: () => import("../views/fatherAndSon/fatherToSon.vue"),
        },
        {
          path: "/sys",
          name: "sys",
          meta:{
            title:"系统设置",
          },
          component: () => import("../views/system.vue"),
        },
      ]
    },
    {
      path: '/:pathMatch(.*)*',
      name: '404',
      component: () => import('../components/404.vue'),
    }
  ]
})

export default router
